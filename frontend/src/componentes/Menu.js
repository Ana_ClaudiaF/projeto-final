import React from 'react';

import Filme from '../componentes/Filme';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

class Menu extends React.Component {

    constructor(props) {
        super(props)

        this.state = { itemNavegar: null }
    }

    navegar = (page) => {
        switch (page) {
            case 1: {
                this.setState({ itemNavegar: <Filme/> })
                break
            }

            default: {
                this.setState({ itemNavegar: null })
                break
            }
        }
    }

    render() {

        return (

            <div>

                <Navbar bg="dark" variant="dark" expand="sm">
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link onClick={this.navegar.bind(this, 1)}>Filmes</Nav.Link>
                            <Nav.Link onClick={this.navegar.bind(this, 2)}>Estúdios</Nav.Link>
                            <Nav.Link onClick={this.navegar.bind(this, 3)}>Proutores</Nav.Link>
                            <Nav.Link onClick={this.navegar.bind(this, 4)}>Personagens</Nav.Link>
                            <Nav.Link onClick={this.navegar.bind(this, 5)}>Editores</Nav.Link>
                            <Nav.Link onClick={this.navegar.bind(this, 6)}>Usuários</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                {this.state.itemNavegar}

            </div>
        )
    }
}

export default Menu
