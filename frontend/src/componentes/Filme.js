import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Badge from 'react-bootstrap/Badge';

class Filme extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            name: this.props.name,
            year: this.props.year,
            genres: this.props.genres,
            studio: this.props.studio,
            producers: this.props.producers,
            cast: this.props.cast,
            alert: ""
        }
    }

    handleChange = (event) => {
        const inputName = event.target.nome
        const inputValue = event.target.value
        this.setState({ [inputName]: inputValue })

    }
    
    alert = () => {
        if (this.state.nome == "" || this.state.ano == "") {
            this.setState({alert: "Preencha os campos nome e ano!"});
        }
    }


    handleClick = () => {
        this.alert();
        var data = {
            "nome": this.state.name,
            "produtor": this.state.producers,
            "ano": this.state.year,
            "editor": this.state.editor,
            "genero": this.state.genres,
            "elenco": this.state.cast
        }
        
        this.props.handleAction(data)
    }

    render() {
        return (
            <div>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <Form onSubmit={this.handleClick}>
                    <Form.Row className="justify-content-md-center">
                        <Col sm="6">
                            <Form.Label >Nome</Form.Label>
                            <Form.Control name="nome" value={this.state.name} onChange={this.handleChange} />
                        </Col>

                        <Col sm="2">
                            <Form.Label>Ano</Form.Label>
                            <Form.Control name="ano" value={this.state.year} onChange={this.handleChange} />
                        </Col>

                        <Col sm="6">
                            <Form.Label>Genero</Form.Label>
                            <Form.Control value={this.state.genres} name="genero" type="text" onChange={this.handleChange} />
                        </Col>

                        <Col sm="6">
                            <Form.Label>Estúdio</Form.Label>
                            <Form.Control value={this.state.studio} name="estudio" onChange={this.handleChange} />
                        </Col>

                        <Col sm="6">
                            <Form.Label>Produtor</Form.Label>
                            <Form.Control value={this.state.producers} name="produtor" onChange={this.handleChange} />
                        </Col>

                        <Col sm="6">
                            <Form.Label>Elenco</Form.Label>
                            <Form.Control value={this.state.cast} name="elenco" onChange={this.handleChange} />
                        </Col>

                        <Button varient="default" onClick={this.handleClick}>Cadastrar</Button>
                    </Form.Row >
                </Form>

                <Badge position="static" variant="danger">{this.state.alert}</Badge>

                <br>
                </br>
            </div >
        )

    }
}

export default Filme
