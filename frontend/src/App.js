import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Menu from './componentes/Menu';


function App() {
  return (
    <div className="App">
      <Menu></Menu>
    </div>
  );
}

export default App;
