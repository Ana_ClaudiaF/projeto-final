const mongoose = require("../data/connection");

const editorSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    age:{
        type: Number
    },
    studio:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Studio',
        default: null
    },
    film:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Film',
        default: null
    },
    createAt:{
        type: Date,
        default: Date.now
    }
});

const editors = mongoose.model('Editors', editorSchema);

module.exports = editors;