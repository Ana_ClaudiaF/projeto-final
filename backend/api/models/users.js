const mongoose = require("../data/connection");

const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true,
        select: false
    },
    createAt:{
        type: Date,
        default: Date.now
    }
});

const user = mongoose.model('User', userSchema);

module.exports = user;