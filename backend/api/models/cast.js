const mongoose = require("../data/connection");

const castSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    age:{
        type: Number,
        required: false
    },
    createAt:{
        type: Date,
        default: Date.now
    }
});

const elenco = mongoose.model('Cast', castSchema);

module.exports = elenco;