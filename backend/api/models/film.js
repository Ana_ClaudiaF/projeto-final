const mongoose = require("../data/connection");

const filmSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    year:{
        type: Date
    },
    genres:{
        type:String
    },
    studio:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Studio',
        default: null
    },
    producers:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Producers',
        default: null
    },
    cast:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Cast',
        default: null
    },
    createAt:{
        type: Date,
        default: Date.now
    }
});

const film = mongoose.model('Film', filmSchema);

module.exports = film;