const mongoose = require("../data/connection");

const producersSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    age:{
        type: Number
    },
    studio:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Studio',
        default: null
    },
    film:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Film',
        default: null
    },
    createAt:{
        type: Date,
        default: Date.now
    }
});

const producers = mongoose.model('Producers', producersSchema);

module.exports = producers;