const mongoose = require("../data/connection");

const studioSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    group:{
        type: String
    }, 
    producers:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Producers',
        default: null
    },
    createAt:{
        type: Date,
        default: Date.now
    }
});

const estudio = mongoose.model('Studio', studioSchema);

module.exports = estudio;