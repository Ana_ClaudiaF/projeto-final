const express = require('express');
const router = express.Router();
const cast = require('../models/cast');

//CREATE
router.post("/save", (req, res) => {
	    var savecast = new cast(req.body);

	    savecast.save((err, doc) => {
		    if (err) {console.log(err);
			    res.status(400).json({ error: "Problemas ao salvar o elenco" });
		    }

	        res.status(200).json(doc);
	    });
});

//READ
router.get("/all", (req, res) => {console.log(req)
	var limit = Number(req.query.limit);
	
	try{
		cast.find().limit(limit).then((doc) =>{
			res.status(200).json(doc);
		});
	} catch(err){
		res.status(400).json({ error: "Erro durante a busca do elenco" })
	}
});

router.get("/all/:id", (req, res) => {
	var id = req.params.id;

	cast.findById(id, (err, doc) =>{
		if (err){
			res.status(400).json({ error: "O código " + id + " do elenco é inexistente" });
		}
		
		res.status(200).json(doc);
	});
});

//UPDATE
router.put('/update/:id', (req, res) => {console.log("A")
    var id = req.params.id;
	var updte = req.body;
	
    cast.findOneAndUpdate(id, updte, (err, doc) => {
        if (err) {
           return res.status(400).send({ error: "Problemas ao alterar o elenco" });
        }
        
        res.status(200).json(doc);
    });
});

//DELETE
router.delete('/delete/:id', (req, res) => {
    var id = req.params.id;
    
    cast.findOneAndDelete(id, (err, doc) => {
        if (err) {
            res.status(400).json({ error: "Problemas ao excluir o elenco" });
        }
        
        res.status(200).json(doc);
    });
});

module.exports = router;