const express = require('express');
const router = express.Router();
const studio = require('../models/studio');

//CREATE
router.post("/save", (req, res) => {
	    var savestudio = new studio(req.body);

	    savestudio.save((err, doc) => {
		    if (err) {console.log(err);
			    res.status(400).json({ error: "Problemas ao salvar o estúdio" });
		    }

	        res.status(200).json(doc);
	    });
});

//READ
router.get("/all", (req, res) => {console.log(req)
	var limit = Number(req.query.limit);
	
	try{
		studio.find().limit(limit).then((doc) =>{
			res.status(200).json(doc);
		});
	} catch(err){
		res.status(400).json({ error: "Erro durante a busca do studioe" })
	}
});

router.get("/all/:id", (req, res) => {
	var id = req.params.id;

	studio.findById(id, (err, doc) =>{
		if (err){
			res.status(400).json({ error: "O código " + id + " do studioe é inexistente" });
		}
		
		res.status(200).json(doc);
	});
});

//UPDATE
router.put('/update/:id', (req, res) => {console.log("A")
    var id = req.params.id;
	var updte = req.body;
	
    studio.findOneAndUpdate(id, updte, (err, doc) => {
        if (err) {
           return res.status(400).send({ error: "Problemas ao alterar o estúdio" });
        }
        
        res.status(200).json(doc);
    });
});

//DELETE
router.delete('/delete/:id', (req, res) => {
    var id = req.params.id;
    
    studio.findOneAndDelete(id, (err, doc) => {
        if (err) {
            res.status(400).json({ error: "Problemas ao excluir o estúdio" });
        }
        
        res.status(200).json(doc);
    });
});

module.exports = router;