const express = require('express');

const auth = require("../data/authMiddleware");

const router = express.Router();

router.use(auth);

router.get("/", (req, res) =>{
    res.send({ok: true});
});

module.exports = router;