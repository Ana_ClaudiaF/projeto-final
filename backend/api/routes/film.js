const express = require('express');
const router = express.Router();
const film = require('../models/film');
let middleware = require('../data/authMiddleware');

//CREATE
router.post("/save", (req, res) => {
	    var saveFilm = new film(req.body);

	    saveFilm.save((err, doc) => {
		    if (err) {console.log(err);
			    res.status(400).json({ error: "Problemas ao salvar o filme" });
		    }

	        res.status(200).json(doc);
	    });
});

//READ
router.get("/all", (req, res) => {console.log(req)
	var limit = Number(req.query.limit);
	
	try{
		film.find().limit(limit).then((doc) =>{
			res.status(200).json(doc);
		});
	} catch(err){
		res.status(400).json({ error: "Erro durante a busca do filme" })
	}
});

router.get("/all/:id", (req, res) => {
	var id = req.params.id;

	film.findById(id, (err, doc) =>{
		if (err){
			res.status(400).json({ error: "O código " + id + " do filme é inexistente" });
		}
		
		res.status(200).json(doc);
	});
});

//UPDATE
router.put('/update/:id', (req, res) => {console.log("A")
    var id = req.params.id;
	var updte = req.body;
	
    film.findOneAndUpdate(id, updte, (err, doc) => {
        if (err) {
           return res.status(400).send({ error: "Problemas ao alterar o filme" });
        }
        
        res.status(200).json(doc);
    });
});

//DELETE
router.delete('/delete/:id', (req, res) => {
    var id = req.params.id;
    
    film.findOneAndDelete(id, (err, doc) => {
        if (err) {
            res.status(400).json({ error: "Problemas ao excluir o filme" });
        }
        
        res.status(200).json(doc);
    });
});

module.exports = router;