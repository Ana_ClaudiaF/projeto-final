const express = require('express');
const router = express.Router();
const producers = require('../models/producers');
let middleware = require('../data/authMiddleware');

//CREATE
router.post("/save", (req, res) => {
	    var saveproducers = new producers(req.body);

	    saveproducers.save((err, doc) => {
		    if (err) {console.log(err);
			    res.status(400).json({ error: "Problemas ao salvar o produtores" });
		    }

	        res.status(200).json(doc);
	    });
});

//READ
router.get("/all", (req, res) => {console.log(req)
	var limit = Number(req.query.limit);
	
	try{
		producers.find().limit(limit).then((doc) =>{
			res.status(200).json(doc);
		});
	} catch(err){
		res.status(400).json({ error: "Erro durante a busca do produtores" })
	}
});

router.get("/all/:id", (req, res) => {
	var id = req.params.id;

	producers.findById(id, (err, doc) =>{
		if (err){
			res.status(400).json({ error: "O código " + id + " do produtores é inexistente" });
		}
		
		res.status(200).json(doc);
	});
});

//UPDATE
router.put('/update/:id', (req, res) => {console.log("A")
    var id = req.params.id;
	var updte = req.body;
	
    producers.findOneAndUpdate(id, updte, (err, doc) => {
        if (err) {
           return res.status(400).send({ error: "Problemas ao alterar o produtores" });
        }
        
        res.status(200).json(doc);
    });
});

//DELETE
router.delete('/delete/:id', (req, res) => {
    var id = req.params.id;
    
    producers.findOneAndDelete(id, (err, doc) => {
        if (err) {
            res.status(400).json({ error: "Problemas ao excluir o produtores" });
        }
        
        res.status(200).json(doc);
    });
});

module.exports = router;