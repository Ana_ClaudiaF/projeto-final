## Projeto APIRest

Implementação de uma APIRest com Node.js integrado ao MongoDB, implementação
feita para a disciplina de JPW 2019/2

## Endpoints

## Film (Filme)

## Descrição

Permite a adição, atualização, pesquisa e exclusão de um filme.

#### URI Padrão

http://localhost:3000/api/filmes

#### Atributos

| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do recurso |
| name | String | Nome do filme |
| year | Date | Ano em que o filme foi lançado |
| genres | String | Gênero do filme |
| studio | studio | Referencia ao estudio de criação |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `http://localhost:3000/api/filmes/all` | Retorna todos os os recursos do Filme |
| GET | `http://localhost:3000/api/filmes/all/:id` | Retorna o recurso do filme pelo `id` |
| POST | `http://localhost:3000/api/filmes/save` | Insere um novo recurso |
| PUT | `http://localhost:3000/api/filmes/update/:id` | Atualiza o recurso Entidade01 pelo `id` |
| DELETE | `http://localhost:3000/api/filmes/delete/:id` | Deleta o recurso Entidade01 pelo `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |

## Cast (Elenco)

## Descrição

Permite a adição, atualização, pesquisa e exclusão de um elenco de um filme.

#### URI Padrão

http://localhost:3000/api/elenco

#### Atributos

| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do recurso |
| name | String | Nome do filme |
| age | Number | Idade do ator/atriz |
| createAt | Date | Data da criação do cadastro |


#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `http://localhost:3000/api/elenco/all` | Retorna todos os os recursos do Filme |
| GET | `http://localhost:3000/api/elenco/all/:id` | Retorna o recurso do filme pelo `id` |
| POST | `http://localhost:3000/api/elenco/save` | Insere um novo recurso |
| PUT | `http://localhost:3000/api/elenco/update/:id` | Atualiza o recurso Entidade01 pelo `id` |
| DELETE | `http://localhost:3000/api/elenco/delete/:id` | Deleta o recurso Entidade01 pelo `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |

## Characters (Personagens)

## Descrição

Permite a adição, atualização, pesquisa e exclusão de um personagem.

#### URI Padrão

http://localhost:3000/api/personagens

#### Atributos

| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do recurso |
| name | String | Nome do personagem |
| cast | cast | Referencia ao ator/atriz que fez o personagem |
| film | film | Referencia ao filme em que o personagem aparece |
| createAt | Date | Data do cadastro do filme |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `http://localhost:3000/api/personagens/all` | Retorna todos os os recursos do Filme |
| GET | `http://localhost:3000/api/personagens/all/:id` | Retorna o recurso do filme pelo `id` |
| POST | `http://localhost:3000/api/personagens/save` | Insere um novo recurso |
| PUT | `http://localhost:3000/api/personagens/update/:id` | Atualiza o recurso Entidade01 pelo `id` |
| DELETE | `http://localhost:3000/api/personagens/delete/:id` | Deleta o recurso Entidade01 pelo `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |

## Editors (Editores)

## Descrição

Permite a adição, atualização, pesquisa e exclusão de um editor de filme.

#### URI Padrão

http://localhost:3000/api/editores

#### Atributos

| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do recurso |
| name | String | Nome do editor |
| studio | studio | Estúdio para qual trabalha |
| film | film | Filme que editor |
| createAt | Date | Data do cadastro do filme |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `http://localhost:3000/api/editores/all` | Retorna todos os os recursos do Filme |
| GET | `http://localhost:3000/api/editores/all/:id` | Retorna o recurso do filme pelo `id` |
| POST | `http://localhost:3000/api/editores/save` | Insere um novo recurso |
| PUT | `http://localhost:3000/api/editores/update/:id` | Atualiza o recurso Entidade01 pelo `id` |
| DELETE | `http://localhost:3000/api/editores/delete/:id` | Deleta o recurso Entidade01 pelo `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |

## Producers (Produtores)

## Descrição

Permite a adição, atualização, pesquisa e exclusão de um produtor de filme.

#### URI Padrão

http://localhost:3000/api/produtores

#### Atributos

| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do recurso |
| name | String | Nome do editor |
| studio | studio | Estúdio para qual trabalha |
| film | film | Filme que editor |
| createAt | Date | Data do cadastro do filme |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `http://localhost:3000/api/produtores/all` | Retorna todos os os recursos do Filme |
| GET | `http://localhost:3000/api/produtores/all/:id` | Retorna o recurso do filme pelo `id` |
| POST | `http://localhost:3000/api/produtores/save` | Insere um novo recurso |
| PUT | `http://localhost:3000/api/produtores/update/:id` | Atualiza o recurso Entidade01 pelo `id` |
| DELETE | `http://localhost:3000/api/produtores/delete/:id` | Deleta o recurso Entidade01 pelo `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |

## Users (Usuários)

## Descrição

Permite a adição, atualização, pesquisa e exclusão de um usuário.

#### URI Padrão

http://localhost:3000/api/usuarios

#### Atributos

| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do recurso |
| name | String | Nome do editor |
| password | String | Senha para autenticação |
| createAt | Date | Data do cadastro do filme |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| POST | `http://localhost:3000/api/usuarios/save` | Insere um novo recurso |
| POST | `http://localhost:3000/api/usuarios/authenticate` | Autentica um usuário cadastrado |


#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |

## Autenticação

Não finalizado

O processo de autenticação se dá por meio do arquivo authMiddleware.js,
a autenticação basicamente consiste em autenticar um usuário cadastrado através
do token que é gerado durante o processo de cadastro de um usuário, ele só
deixa passar p/ a próxima rota caso o usuário seja existente e o token dele
seja válido.